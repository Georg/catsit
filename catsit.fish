function catsit
	set pipe /run/catsit/catsitd
	if [ -p $pipe ]
		/usr/local/sbin/catsit -c $pipe $argv
	end
	if [ ! -p $pipe ]
		echo "Connection impossible."
	end
end
