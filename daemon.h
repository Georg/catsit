/* Copyright (C) 2020  C. McEnroe <june@causal.agency>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

typedef unsigned char byte;

extern struct Prepend {
	size_t cap, len;
	char **commands;
} prepend;

static inline void prependClear(void) {
	for (size_t i = 0; i < prepend.len; ++i) {
		free(prepend.commands[i]);
	}
	prepend.len = 0;
}
static inline int prependAdd(const char *command) {
	if (prepend.len == prepend.cap) {
		size_t cap = (prepend.cap ? prepend.cap * 2 : 8);
		void *ptr = reallocarray(
			prepend.commands, cap, sizeof(*prepend.commands)
		);
		if (!ptr) return -1;
		prepend.cap = cap;
		prepend.commands = ptr;
	}
	prepend.commands[prepend.len] = strdup(command);
	if (!prepend.commands[prepend.len]) return -1;
	prepend.len++;
	return 0;
}

enum { LineCap = 512 };
struct Line {
	size_t len;
	char buf[LineCap];
};

static inline const char *lineFlush(struct Line *line) {
	if (!line->len) return NULL;
	line->buf[line->len++] = '\0';
	return line->buf;
}

static inline const char *lineRead(struct Line *line, int fd) {
	char *nul = memchr(line->buf, '\0', line->len);
	if (nul) {
		nul++;
		line->len -= nul - line->buf;
		memmove(line->buf, nul, line->len);
	}

	size_t cap = sizeof(line->buf) - line->len - 1;
	if (!cap) return lineFlush(line);

	ssize_t len = read(fd, &line->buf[line->len], cap);
	if (len < 0 && errno != EAGAIN) return NULL;
	if (len > 0) line->len += len;

	char *nl = memchr(line->buf, '\n', line->len);
	if (nl) {
		*nl = '\0';
		return line->buf;
	} else {
		errno = EAGAIN;
		return NULL;
	}
}

enum {
	SHELL,
	PATH,
	USER,
	HOME,
	EnvironNull,
	EnvironLen,
};

extern const char *serviceDir;
extern uid_t serviceUID;
extern gid_t serviceGID;
extern char *serviceEnviron[EnvironLen];

enum State {
	Stop,
	Start,
	Restart,
};

struct Service {
	char *name;
	char *command;
	bool privileged;
	enum State intent;
	enum State state;
	pid_t pid;
	int outPipe[2];
	int errPipe[2];
	struct Line outLine;
	struct Line errLine;
	struct timespec startTime;
	struct timespec restartInterval;
	struct timespec restartDeadline;
};

extern struct Services {
	size_t cap, len;
	struct Service *ptr;
} services;

int serviceAdd(const char *name, const char *command);
void serviceDrop(size_t index);
void serviceStatus(struct Service *service);
void serviceStart(struct Service *service);
void serviceStop(struct Service *service);
void serviceRestart(struct Service *service);
void serviceSignal(struct Service *service, int signal);
void serviceRead(struct Service *service);
void serviceReap(pid_t pid, int status);

struct Set256 {
	uint32_t bits[8];
};
static inline void setClear(struct Set256 *set) {
	for (int i = 0; i < 8; ++i) set->bits[i] = 0;
}
static inline void setAdd(struct Set256 *set, byte x) {
	set->bits[x / 32] |= 1 << (uint32_t)(x & 31);
}
static inline uint32_t setTest(const struct Set256 *set, byte x) {
	return set->bits[x / 32] & (1 << (uint32_t)(x & 31));
}

extern struct Set256 stopExits;
extern struct timespec restartInterval;
extern struct timespec resetInterval;
