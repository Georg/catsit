## Catsit

Upstream information: https://git.causal.agency/catsit/about/

## Linux port
##### Why?
I found myself wanting to migrate my Pounce-based IRC bouncers to a different machine. Previously I run them on a systemd based distribution, but as the new machine would be rc.d based, I figured I will use catsit to supervise my bouncers. I did not expect this to be an issue, as I used various other Causal Agency IRC software (pounce, calico, litterbox, catgirl) before, which always worked flawlessly, however after preparing the new Slackware box I found catsit, the only program in the suite I have not yet tried, to be BSD only. Now I am a fan of BSD based operating systems, and run many myself, but I did not want to move my IRC box again - hence found myself hammering catsit until it would run on Slackware - which turned out to take longer than several OpenBSD installations.

##### Dependencies
This needs ...

[libbsd](https://libbsd.freedesktop.org/wiki/) - could likely do with native POSIX libraries

[libkqueue](https://github.com/mheily/libkqueue) - probably wiser to port this to libevent or libuv

[libmd](https://www.hadrons.org/software/libmd/) - I forgot why

... in addition to `make` and some version of `gcc` (tested with 11.2.0).

##### Disclaimer
I recommend AGAINST using this fork. The patches are horrible, disgraceful, a shame to humanity, and heavily insulting towards the upstream developer.
