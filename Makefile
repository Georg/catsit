UNAME != uname

PREFIX ?= /usr/local
MANDIR ?= ${PREFIX}/man
RUNDIR ?= /var/run
ETCDIR ?= ${PREFIX}/etc

CFLAGS += -std=c99 -Wall -Wextra -Wpedantic
CFLAGS += -D'ETCDIR="${ETCDIR}"' -D'RUNDIR="${RUNDIR}"' -I'/usr/local/include/kqueue' -I'/usr/local/include' -D'_POSIX_C_SOURCE=199309L' -u,libbsd_init_func -lbsd-ctor -lbsd -lkqueue

RC_SCRIPT = rc.catsit

-include config.mk

BINS = catsit-timer catsit-watch
SBINS = catsit catsitd
MANS = ${BINS:=.1} catsit.conf.5 ${SBINS:=.8}

OBJS = daemon.o service.o

dev: tags all

all: ${BINS} ${SBINS}

catsitd: ${OBJS}
	${CC} ${CFLAGS} ${LDFLAGS} ${OBJS} ${LDLIBS} -o $@

${OBJS}: daemon.h

.SUFFIXES: .in

.in:
	sed -e 's|%%PREFIX%%|${PREFIX}|g' -e 's|%%RUNDIR%%|${RUNDIR}|g' $< > $@
	chmod a+x $@

tags: *.[ch]
	ctags -w *.[ch]

clean:
	rm -f ${BINS} ${SBINS} ${OBJS} ${RC_SCRIPT} tags

install: ${BINS} ${SBINS} ${MANS}
	install -d ${DESTDIR}${PREFIX}/bin
	install -d ${DESTDIR}${PREFIX}/sbin
	install -d ${DESTDIR}${ETCDIR}/rc.d
	install ${BINS} ${DESTDIR}${PREFIX}/bin
	install ${SBINS} ${DESTDIR}${PREFIX}/sbin
	for section in 1 5 8; do install -d ${DESTDIR}${MANDIR}/man$$section; done
	for manpage in ${MANS}; do install -m 444 $$manpage ${DESTDIR}${MANDIR}/man"$${manpage##*.}"; done
	install ${RC_SCRIPT} ${DESTDIR}${ETCDIR}/rc.d

uninstall:
	for bin in ${BINS}; \
	do \
		rm ${DESTDIR}${PREFIX}/bin/$$bin ; \
	done ; \
	for sbin in ${SBINS} ; \
		do \
		rm ${DESTDIR}${PREFIX}/sbin/$$sbin ; \
	done ; \
	for man in ${MANS} ; \
		do \
		rm ${DESTDIR}${MANDIR}/man*/$$man ; \
	done ; \
	rm ${DESTDIR}${ETCDIR}/rc.d/${RC_SCRIPT}





